#!/usr/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

image=true
while [[ $# -gt 0 ]]; do
  case $1 in
    --noimage)
      image=false
      shift # past argument
      ;;
  esac
done

set -xe

arch=$(arch)

if [ $image = true ]; then
  automotive-image-builder --verbose --container \
    --include=/var/lib/containers/storage/ \
    build \
    --distro autosd9 \
    --target qemu \
    --mode image \
    --build-dir=_build \
    --export image \
    container_remote.aib.yml \
    container_remote.$arch.img
fi
