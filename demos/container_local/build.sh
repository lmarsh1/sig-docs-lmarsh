#!/usr/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

image=true
while [[ $# -gt 0 ]]; do
  case $1 in
    --noimage)
      image=false
      shift # past argument
      ;;
  esac
done


set -xe

if [ ! -d "/var/tmp/my_repo" ]; then
    echo "Running rpm_local to generate the RPM and RPM repository needed for this stage"
    pushd ../rpm_local
    sh build.sh --noimage
    popd
fi

# First copy locally the RPM repository
# This is required as we cannot copy into the container files that are local but
# not placed in the folder where the container is built.
cp -r /var/tmp/my_repo .

source /etc/os-release

sid="autosd"
if [ $ID == "fedora" ]; then
  sid="f"
fi

sudo podman build -t localhost/auto-apps:latest -f Containerfile.$sid$VERSION_ID

sudo podman run -it auto-apps rpm -q auto-apps
p=$PATH
arch=$(arch)


if [ $image = true ]; then

   # We need to automotive-image-builder with sudo here so that it can
   # read/access /var/lib/containers/storage to retrieve the images built
   # here above.
   sudo env "PATH=$PATH" automotive-image-builder --verbose --container \
    --include=/var/lib/containers/storage/ \
    build \
    --distro autosd9 \
    --target qemu \
    --mode image \
    --build-dir=_build \
    --export image \
    container_local.aib.yml \
    container_local.$arch.img

   u=$USER
   sudo chown $u:$u *.img
fi
