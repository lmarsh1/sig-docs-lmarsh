#!/bin/bash

set -ex

SSH_PORT=${1:-2222}
arch=$(arch)

cat > test_script.expect << EOF
#!/bin/expect

# Wait enough (forever) until a long-time boot
set timeout 120

# Start the guest VM
spawn automotive-image-runner --ssh-port $SSH_PORT --nographics minimal_qm.$arch.img

expect "localhost login: "
send "root\n"

set timeout 3

expect "Password: "
send "password\n"

expect "# "
send "rpm -q qm\n"
send "systemctl is-active qm\n"
expect {
  "active" { send "poweroff\n"; expect eof }
  timeout     { exit 3 }
}

EOF

expect -f test_script.expect
