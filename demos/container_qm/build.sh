#!/usr/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

image=true
while [[ $# -gt 0 ]]; do
  case $1 in
    --noimage)
      image=false
      shift # past argument
      ;;
  esac
done


set -xe

if ! sudo podman image exists quay.io/centos/centos:stream10 ; then
    sudo podman pull quay.io/centos/centos:stream10
else
    echo "Container image found"
fi

p=$PATH
arch=$(arch)

if [ $image = true ]; then
   sudo env "PATH=$PATH" automotive-image-builder --verbose --container \
   --include=/var/lib/containers/storage/ \
    build \
    --distro autosd9 \
    --target qemu \
    --mode image \
    --build-dir=_build \
    --export image \
    container_qm.aib.yml \
    container_qm.$arch.img

   u=$USER
   sudo chown $u:$u *.img
fi
