#!/usr/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

image=true
while [[ $# -gt 0 ]]; do
  case $1 in
    --noimage)
      image=false
      shift # past argument
      ;;
  esac
done


set -xe

source /etc/os-release

sid="autosd"
if [ $ID == "fedora" ]; then
  sid="f"
fi

for name in `ls containers`;
do
sudo podman build -t localhost/$name:latest -f containers/$name/Containerfile
done

arch=$(arch)

if [ $image = true ]; then
   sudo env "PATH=$PATH" automotive-image-builder --verbose --container \
   --include=/var/lib/containers/storage/ \
    build \
    --distro autosd9 \
    --target qemu \
    --mode image \
    --build-dir=_build \
    --export image \
    ipc_between_qm_asil_2.aib.yml \
    ipc_between_qm_asil_2.$arch.img

   u=$USER
   sudo chown $u:$u *.img

fi
