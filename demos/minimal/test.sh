#!/bin/bash

SSH_PORT=${1:-2222}
arch=$(arch)

cat > test_script.expect << EOF
#!/bin/expect

# Wait enough (forever) until a long-time boot
set timeout 120

# Start the guest VM
spawn automotive-image-runner --ssh-port $SSH_PORT --nographics minimal.$(arch).img

set timeout 20
expect "localhost login: "
send "root\n"

expect "Password: "
send "password\n"

expect "# "
send "poweroff\n"
expect eof
EOF

expect -f test_script.expect
