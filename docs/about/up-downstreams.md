# AutoSD upstream and downstream

AutoSD is the [upstream](https://www.redhat.com/en/blog/what-open-source-upstream)
binary distribution that serves as the public, in-development preview of Red Hat In-Vehicle Operating System (OS).
AutoSD is downstream of CentOS Stream, so it retains most of the CentOS Stream code with a few divergences, such as an optimized
automotive-specific kernel rather than CentOS Stream's kernel package. Red Hat In-Vehicle OS is based on both AutoSD and RHEL,
both of which are downstreams of CentOS Stream.

![Diagram shows code flow from open source projects, like Linux mainline, to Fedora, Fedora Enterprise Linux Next, or ELN, to CentOS Stream. CentOS Stream receives contributions from many CentOS special interest groups, or SIGs, which then flow to both RHEL and AutoSD. The Automotive SIG contributes AutoSD code back to various open source projects. Finally, RHEL and AutoSD code converges to form Red Hat In-Vehicle OS.](../img/content-flow-diagram.png "AutoSD in the Red Hat In-Vehicle OS upstream and downstream code flow")

Both products integrate a wide variety of open source software. The CentOS Automotive SIG builds and tests uncertified AutoSD packages.
All the same SPEC files and patches for all the components
that Red Hat uses to build its downstream product are available and retrievable from git,
whether they come from CentOS Stream or AutoSD.
For more information, see [Open source development](../features-and-concepts/con_open-source-development.md).

During downstream development, Red Hat follows the same upstream-first, downstream-flow
process as CentOS Stream has with AutoSD and Red Hat Enterprise Linux (RHEL).
Red Hat engineering teams track packages in the upstream AutoSD project to identify those that are relevant to
automotive safety use cases, integrate that AutoSD code with RHEL rather than CentOS Stream, and
then perform the rigorous testing, maintenance,
and evidence generation required to continuously certify Red Hat In-Vehicle OS for functional safety (FuSa).
For more information, see [Downstream continuous certification](con_continuous-certification.md).

To participate in open source automotive innovation and shape Red Hat In-Vehicle OS with Red Hat, join the
[CentOS Automotive SIG](https://sigs.centos.org/automotive/) and contribute to [AutoSD](https://autosd.sig.centos.org/). Read the
[Automotive SIG documentation](https://sigs.centos.org/automotive/) to get started
[enabling hardware](../hardware-enablement/index.md) or building and customizing [packages](../building/packaging_apps_with_rpm.md),
[containers](../building/containerizing_apps.md), and [images](../building/building_an_os_image.md) .
