# A more reactive, deterministic Real-time Linux kernel

AutoSD uses [Real-Time Linux](https://wiki.linuxfoundation.org/realtime/start) within its automotive kernel. The real-time automotive kernel
offers the advantages of determinism and preemption over the standard Linux kernel through its use of the `PREEMPT_RT` feature set, Real-Time
Scheduler, and POSIX clocks. This allows AutoSD to maintain low latency and a consistent response times for time-sensitive applications and
guarantee the execution of high-priority, safety-critical tasks.

## Virtual memory management with `mlock` and `munlock`

The real-time kernel allocates memory by breaking physical memory into chunks, called pages, and then maps them to the virtual memory. Real-time
systems use a virtual memory system, where an address referenced by a user-space application translates into a physical address. An advantage of
having the translation mechanism in between a program and the actual memory is that the OS can swap pages when required or on request of the CPU.
Faults in real-time occur when a process needs a specific page that is not mapped or is no longer available in the memory.

* Minor faults occur in real-time when a process attempts to access a portion of memory before it has been initialized.
* Major faults occur in real-time when the system has to either synchronize memory buffers with the disk, swap memory pages belonging to other
processes, or undertake any other input output (I/O) activity to free memory. This occurs when the processor references a virtual memory address
that does not have a physical page allocated to it.

The memory lock (`mlock()`) system calls lock or unlock a specified range of the address space and prevent Linux from paging the locked memory to
swap space. The `mlock()` and `munlock()` system calls lock and unlock a specified range of process address pages. The pages in the specified range
remain resident in the memory until the `munlock()` system call unlocks the pages.

## Preemption of `mlock` for faster service of higher priority interrupts

Preemption can have a particularly negative impact on performance, and constant preemption can lead to a state known as thrashing. This problem
occurs when processes are constantly preempted and no process ever gets to run completely. Changing the priority of a task can help reduce
involuntary preemption.

The Real-Time Linux kernel modifies the way interrupts are handled to improve performance and decrease latency. The biggest change that the
`PREEMPT_RT` feature makes to the standard Linux kernel is that it does _not_ disable local interrupts while a memory lock is held. This allows
interrupts to be serviced, allowing the task holding the lock to be preempted. The result is that high-priority tasks have the opportunity to run
sooner after their condition is raised than on a stock kernel, where they would have to wait in until the blocking task’s lock is released.

## Decreased latency for high-priority tasks with Real-Time scheduler

In the Real-Time Linux kernel, the scheduler is the kernel component that determines the runnable thread to run. Each thread has an associated
scheduling policy and a static scheduling priority, known as `sched_priority`. The scheduling is preemptive and therefore the currently running
thread stops when a thread with a higher static priority gets ready to run. The running thread then returns to the `waitlist` for its static
priority. Real-time threads have higher priority than the standard threads, so `kernel-automotive` can use the higher privileges of the Real-Time
scheduler to guarantee the priority-inheritance chain, avoid priority inversion, and execute high-priority tasks within the specified timeframe
once the condition is met.

## Reduced CPU cost with POSIX clocks

The Real-Time Linux kernel, a optimizes performance by using POSIX clocks with the `clock_gettime()` function to produce clock readings with the
lowest possible CPU cost.

## An automotive scenario with the Real-Time Linux kernel

Suppose the engine sends an electrical signal to the ECU that there is a problem, which raises an interrupt signal. This leads to the kernel
scheduling an application that displays the check engine light on the dashboard display. The Real-Time Linux kernel minimizes the risk that when
the ECU signals the interrupt that the kernel was executing a process it could not preempt, which would have delayed the application that lit the
icon. Because the kernel could preempt that task, it could immediatly respond to the interrupt, run the application, display the check engine light,
and notify the driver of the engine problem.
