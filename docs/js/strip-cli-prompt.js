// Initialize ClipboardJS for elements with the .md-clipboard class
var clipboard = new ClipboardJS(".md-clipboard", {
    text: function (trigger) {
        // Get the target code block using the data-clipboard-target attribute
        const targetSelector = trigger.getAttribute("data-clipboard-target");
        const codeBlock = document.querySelector(targetSelector);

        if (!codeBlock) {
            console.warn("Code block not found for target:", targetSelector);
            return "";
        }

        // Fetch the text content
        let clipboardText = codeBlock.textContent || "";
        console.log("Original text:", clipboardText);

        // Check if the parent container has a relevant language class
        const preElement = codeBlock.closest("pre");
        const highlightDiv = preElement?.parentElement;
        const languageClasses = ["language-bash", "language-console", "language-shell"];
        const isTargetLanguage = highlightDiv && languageClasses.some(cls => highlightDiv.classList.contains(cls));

        if (isTargetLanguage) {
            const lines = clipboardText.split(/\r\n|\n|\r/);
            if (lines.length > 0) {
                // Remove CLI prompt (# or $) only from the first line
                lines[0] = lines[0].replace(/^\s*[$#]\s*/, "");
            }
            clipboardText = lines.join("\n");
        }

        console.log("Processed text:", clipboardText);
        return clipboardText;
    }
});

// Handle the success event for copying
clipboard.on("success", function (e) {
    console.log("Copied successfully:", e.text);

    // Visual feedback for successful copy
    e.trigger.classList.toggle("fa-clipboard");
    e.trigger.classList.toggle("fa-check");

    setTimeout(function () {
        e.trigger.classList.toggle("fa-clipboard");
        e.trigger.classList.toggle("fa-check");
    }, 2000);
});

// Handle the error event
clipboard.on("error", function (e) {
    console.error("Copy failed:", e.action, e.trigger);
});
