# Welcome to the Automotive SIG

This site contains general information about the Automotive SIG, as well as how to contribute to the repository and how to build and download images.

The Automotive SIG manages several artifacts:

**Automotive Stream Distribution (AutoSD)**
: The primary deliverable of the Automotive SIG is AutoSD, a binary distribution developed within the SIG that is a
public, in-development preview of the upcoming Red Hat In-Vehicle Operating System (OS). AutoSD is CentOS Stream, with divergences
that meet unique automotive use cases, which might include new, automotive-specific packages and rebuilds or reconfigurations
of existing CentOS Stream packages. For more information about AutoSD, see [AutoSD key features](features-and-concepts/index.md).

**RPM repositories**
: These are RPM repositories produced by the Automotive SIG to enhance AutoSD. New packages or features can be developed and hosted there to expand
the capabilities of AutoSD. For more information, see [Automotive SIG repositories](contributing/index.md).

**Sample images**
: These are images built with [Automotive image builder](https://gitlab.com/CentOS/automotive/src/automotive-image-builder) using packages from AutoSD,
the Automotive SIG repositories, or other sources. They are examples of how to use AutoSD. For more information, see
Working with [Pre-built OS images](getting-started/sample-non-sample-images.md), [Introducing Automotive image builder](getting-started/about-automotive-image-builder.md),
and [Deploying sample apps](getting-started/deploying-sample-apps-containers.md).

For more information about the Automotive SIG charter, members, or goal, see [About the Automotive SIG](community/index.md).

For more information about how to engage with the Automotive SIG, see [Community](community/community-comms-mtgs.md) for
 [communication channels](community/community-comms-mtgs.md#communication-channels) and [meeting information](community/community-comms-mtgs.md#past-meetings).

## AutoSD overview

**Open-source development**
: AutoSD is the upstream binary distribution that serves as the public, in-development preview of Red Hat In-Vehicle Operating
System (OS). AutoSD is downstream of CentOS Stream, so it retains most of the CentOS Stream code with a few divergences, such
as an optimized automotive-specific kernel rather than CentOS Stream’s kernel package. Red Hat In-Vehicle OS is based on both
AutoSD and RHEL, both of which are downstreams of CentOS Stream. For more information, see [AutoSD upstream and downstream](about/up-downstreams.md).

**Binary distribution with RPM Package Manager**
: AutoSD is a binary distribution that consists of precompiled RPM packages that contain components for both the automotive
Linux kernel and user spaces. This means that AutoSD is in a ready-to-run format that you can shape into an OS image and install
without additional compilation. AutoSD users also have visibility into the source of the binary. For more information about binary
distibutions vs source distributions, see [source and binary distributions](features-and-concepts/con_source-and-binary-distributions.md).
Software installation and management in AutoSD is handled by RPM Package Manager (RPM),
a package management system used to manage RPM packages. These packages are architecture-specific collections of compiled binary,
configuration, and documentation files. For more information about RPM, see [RPM Package Manager](features-and-concepts/con_rpm-packages-and-the-rpm-package-manager.md).

**Image-based OS**
: AutoSD is an OSTree-based operating system, meaning that it is image-based, immutable, and supports
atomic A/B updates and rollbacks which are storage- and bandwidth-friendly. For more information, see [OSTree](features-and-concepts/con_ostree.md).

**Extending OSTree tamperproofing with composefs**
: As a content-addressed object store with inherent deduplication and byte-level image diff features, OSTree is both storage and
bandwidth friendly. It also offers the capability to dynamically install containerized applications, which makes it possible to
manage application lifecycles independently from the underlying OS lifecycle. When combined with composefs verification and
Unified Extensible Firmware Interface (UEFI) Secure Boot, AutoSD is tamperproof. For more information, see [tamperproofing](features-and-concepts/con_tamperproof.md).

**Mixed criticality**
: AutoSD uses containers as the foundation for Software-Defined Vehicle architecture. With containers, you can run the entire software stack
on a single operating system while isolating ASIL and QM workloads from each other, and from the rest of the system. Because containers are
fundamentally a process isolation mechanism, you can use this architecture to configure criticality on a process level. AutoSD comes with
a preconfigured mixed-criticality architecture leveraging container technologies to constrains application behaviour across the
ASIL/QM divide. For more information, see [mixed criticality](features-and-concepts/con_mixed-criticality.md).

**Reactive, deterministic kernel optimized for automotive**
: AutoSD incorporates Real-time Linux, Real-Time Scheduler, and POSIX clocks in `kernel-automotive` to satisfy automotive requirements for
high performance, minimal size, low latency, and consistent response times. For more information, see [Real Time](about/con_real-time-linux-kernel.md).

**Continuous FuSa certification**
: When AutoSD flows downstream to Red Hat In-Vehicle OS, Red Hat engineering teams perform additional safety verification and
validation specific to the context of each automotive target hardware reference platform before each release. This means that Red Hat
In-Vehicle OS is working on achieving certification as a safety element out of context (SEooC) up to ASIL B according to ISO 26262 2nd ed.
The safety approach is a tailored, open-source interpretation of ISO 26262 that also considers the informative annex ISO/PAS 8926:2024. For
more information, see [Continuous certification](about/con_continuous-certification.md).
