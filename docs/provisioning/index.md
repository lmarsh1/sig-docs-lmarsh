# Provisioning hardware

You can provision your OS image onto the following hardware types:

* [Texas Instruments](flashing-texas-instruments.md)
* [Renesas R-Car S4](flashing-renesas.md)
* [NXP S32G](flashing-nxp.md)
* [Qualcomm Ride SX 4.0](flashing-images-ride4.md)
* [Raspberry Pi](../building/autosd_pi4.md)
