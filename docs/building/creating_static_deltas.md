# Creating static deltas

OSTree downloads updates from a remote OSTree repository. OSTree repositories contain all the files that compose some version of an
image. When you create and manage images with OSTree, you can also create static deltas. Static deltas are a collection of updates generated
between two OSTree commits. Static deltas optimize updates by fetching only objects from the new commit that do not already exist in the image.
If a static delta is available for a specific transaction, OSTree will automatically use it.

**Prerequisites**

* Two or more OSTree-based images, such as the versions you created in
  [Creating an OSTree-based image](updating_ostree.md#creating-an-ostree-based-image) and [Updating OSTree-based images](updating_ostree.md#updating-ostree-based-images)

**Procedure**

1. Run `ostree log` to get the commit IDs of the available versions:

    ```console
    $ ostree log --repo=ostree-repo cs9/x86_64/<target>-<manifest-name>
    commit 81edee454fec85e3973539a3ce3e343615e07f88238ceb42754151e93f6f6e70
    Parent:  dfc718b4b44d26a607032b0a76066bb844a12b00659770a63ab42179d0c91682
    ContentChecksum:  8c97f932600f97058e9a8e8f225b72a87e03eebf3f4c909eada96475b49a3f71
    Date:  2024-11-04 18:18:09 +0000
    Version: 1.1
    (no subject)

    commit dfc718b4b44d26a607032b0a76066bb844a12b00659770a63ab42179d0c91682
    ContentChecksum:  fdee7dd3d3fbbeada8ccd6941a81a58a96f8b53f38e31721ea659d703ee82671
    Date:  2024-11-04 17:38:04 +0000
    Version: 1
    (no subject)
    ```

    There are two commits. Version 1.1, the most recent commit, has the ID `81edee45`. Its parent commit, `dfc718b4`, matches the commit ID for
    Version 1.

1. Generate a static delta using the following syntax:

    ```console
    $ sudo ostree static-delta generate --repo=<ostree-repo-name> --from=REV --to=REV
    ```

    Where the `REV` values are the commit IDs of the versions for which you want to create a static delta. You can use a partial commit ID,
    for example:

    ```console
    $ sudo ostree static-delta generate --repo=<ostree-repo-name> --from=dfc718b4b --to=81edee454fe
    ```

## Making offline updates

An offline update is an update that you manually apply to your system using a locally stored image. An offline update using a static
delta is an efficient way to update your system because a static delta represents only the objects from a new commit that do not exist in the
current image. The result is a smaller, simpler update. Offline updates are useful in scenarios where bandwidth is limited or there is no network
connection to an OSTree repository.

**Procedure**

1. Update your image, for example by adding two new RPMs: "curl" and "tree", your manifest will therefore look something like:

    ```YAML
    content:
      rpms:
        - [..]
        - curl
        - tree
    ```

1. Build the updated version of your image:

    ```console
    $ sudo ./auto-image-builder.sh build --target qemu --mode image --ostree-repo <ostree-repo-name> \
        --export qcow2 <path>/<manifest-name>.aib.yml <image-name>.repo
    ```

2. Generate static deltas and store them in a target directory:

    ```console
    $ sudo tools/generate-deltas <ostree-repo-name>/ <ostree-repo-name>/updates/
    ```

    Use the `generate-deltas` script in the `tools/` directory to generate update files.
    When you run the script against an OSTree repository, it generates update files that contain per-commit deltas for each of the last three image
    parent commits and a larger update file that contains the full delta for all changes applied to the image between the latest `HEAD` commit and
    up to three prior parent commits.

3. Display the disk usage of the files in the `updates` directory:

    ```console
    $ du -h  <ostree-repo-name>/updates/*
    9.9M <ostree-repo-name>/updates/cs9-x86_64-<target>-<manifest-name>-0eb6b68144e8a66a6e324b2dcc9597a2c6da3733ab7e4ed55ae559b41904fd65-410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7
    c8842576d8daeed.update
    571M <ostree-repo-name>/updates/cs9-x86_64-qemu-developer-410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7c8842576d8daeed.update
    9.9M <ostree-repo-name>/updates/cs9-x86_64-<target>-<manifest-name>-f56f6573b5f5d2a1bf990c9030ee8fefcdd35c6fd5db106e4e7236806483e6d2-410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7
    c8842576d8daeed.update
    ```

    Notice the three delta updates:

    Notice the size of the three delta update files:

    * The 9.9M delta file,`cs9-x86_64-<target>-<manifest-name>-0eb6b681<...>-410b97c4<...>.update`,
    contains the deltas to update a system running commit `0eb6b681` to commit `410b97c4`.
    * At 571M, `cs9-x86_64-<target>-<manifest-name>-410b97c4<...>.update`
    is the largest update file and contains the delta between the first and last commits in the repository.
    * Another 9.9M delta update file, `cs9-x86_64-<target>-<manifest-name>-f56f6573<...>-410b97c4c<...>.update`
    contains the deltas to update a system running commit `f56f6573` to commit `410b97c4`.

4. Run the image:

    ```console
    $ sudo ./automotive-image-builder/automotive-image-runner --publish-dir=<ostree-repo-name> <image-name>.qcow2
    ```

5. After the image boots, log in as `root` using the password `password`.

6. From the VM, run `rpm-ostree status` to get the metadata of the delta update that you want to install.

7. Download the delta update:

    ```console
    $ curl --remote-name http://10.0.2.100/updates/<deployment-name>-<hash-from>-<hash-to>.update
    ```

    For example:

    ```console
    # curl --remote-name http://10.0.2.100/updates/cs9-x86_64-qemu-developer-f56f6573b5f5d2a1bf990c9030ee8fefcdd35c6fd5db106e4e7236806483e6d2-410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7c8842576d8daeed.update
    ```

8. Install the delta update by using `ostree static-delta apply-offline`:

    ```console
    # ostree static-delta apply-offline cs9-x86_64-<target>-<manifest-name>-f56f6573b5f5d2a1bf990c9030ee8fefcdd35c6fd5db106e4e7236806483e6d2-410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7c8842576d8daeed.update
    # rpm-ostree rebase 410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7c8842576d8daeed
    Staging deployment... done
    Changes queued for next boot. Run "systemctl reboot" to start a reboot
    # rpm-ostree status
    State: idle
    Deployments:
      auto-sig:410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7c8842576d8daeed
                      Version: 1.2 (2024-11-11T22:21:43Z)
                       Commit: 410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7c8842576d8daeed

    ● auto-sig:cs9/x86_64/<target>-<manifest-name>
                      Version: 1.1 (2024-11-11T21:51:37Z)
                       Commit: f56f6573b5f5d2a1bf990c9030ee8fefcdd35c6fd5db106e4e7236806483e6d2

      auto-sig:cs9/x86_64/<target>-<manifest-name>
                      Version: 1 (2024-11-11T21:38:47Z)
                       Commit: 0eb6b68144e8a66a6e324b2dcc9597a2c6da3733ab7e4ed55ae559b41904fd65
    ```

    The `rpm-ostree status` command is optional, but it is used here to show that the system has multiple versions
    of the image. The `rpm-ostree rebase` command configures the system to reboot to the next
    version (Version 1.2, commit `410b97c4ca59df58f33f`). If you unintentionally install the wrong delta, you will
    receive an error similar to, `Commit XYZ, which is the delta source, is not in repository`. If this occurs,
    repeat this step using the correct delta.

9. Reboot the VM:

    ```console
    $ systemctl reboot
    ```

10. After the image boots, log in as `root` using the password `password`.

11. From the VM, run `rpm-ostree status` to confirm that you successfully installed the update:

    ```console
    # rpm-ostree status
    State: idle
    Deployments:
      ● auto-sig:410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7c8842576d8daeed
                      Version: 1.2 (2024-11-11T22:21:43Z)
                       Commit: 410b97c4ca59df58f33fce3d1e389a3eb8f7c1367c1afacb7c8842576d8daeed

      auto-sig:cs9/x86_64/<target>-<manifest-name>
                      Version: 1.1 (2024-11-11T21:51:37Z)
                       Commit: f56f6573b5f5d2a1bf990c9030ee8fefcdd35c6fd5db106e4e7236806483e6d2
    ```
