# Deploying applications in the QM partition

The AutoSD architecture isolates quality managed (QM) applications from Automotive Safety Integrity Level (ASIL) applications. This isolation
mechanism is the QM software partition. In this architecture, the QM partition is an imageless container that uses a subsection of the host
filesystem as its root filesystem. This means that you must place your applications in one of two locations:

* Configure OSBuild to copy ASIL applications to `/etc/containers/systemd`
* Configure OSBuild to copy QM applications to `/etc/containers/systemd` inside the QM software partition, which is mounted at `/usr/share/qm/`.
  The resulting path for QM applications is `/usr/share/qm/etc/containers/systemd`.

To configure the placement of applications in the QM partition, create a new `qm` stage in your manifest file, and configure
QM-specific operations in the QM partition.

## Embedding RPM packages in the QM partition

Place all QM-level RPM packages in the QM partition. When you write your automotive image builder manifest, place all QM-specific operations
inside the `qm` stage.

**Prerequisites**

* A QM-level RPM package that you want to install. For example, the `auto-apps` RPM that you created in
[Packaging sample application source code with RPM](packaging_apps_with_rpm.md/#packaging-sample-application-source-code-with-rpm), or
any RPM package available from a remote repository.
* A custom manifest file, such as the file that you created in
[Embedding RPM packages in the AutoSD image](packaging_apps_with_rpm.md#embedding-rpm-packages-from-local-storage-into-the-autosd-image)

**Procedure**

1. To configure your custom automotive image builder manifest to install your `auto-apps` application into the QM partition of the OS image,
include the `qm` stage in your manifest file. Create a `content` section in the the `qm` stage and define the repository
that you want to enable, as well as the RPM that you want to install:

    ```YAML title="automotive image builder manifest"
    qm:
      content:
        repos:
          - id: auto-apps
            baseurl: file:///var/tmp/my_repo
        rpms:
          - auto-apps
    ```

**Next steps**

Now that you have configured your custom automotive image builder manifest to initialize the QM partition and install your RPM package,
you can build your AutoSD image. For more information, see [Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).

## Embedding containerized applications in the QM partition

Place all QM-level containerized applications in the QM partition. When you write your automotive image builder manifest, place all
QM-specific operations inside the `qm` stage.

To configure your custom automotive image builder manifest to install one or more container images in the QM partition, include the
`qm` stage in your manifest file. Create a `content` section in the the `qm` stage and define the source URL, tag, and name of the
container image.

You can view a complete example manifest for QM container configuration in the
[demos/container_qm/container_qm.aib.yml](https://gitlab.com/CentOS/automotive/sig-docs/-/tree/main/demos/container_qm/container_qm.aib.yml?ref_type=heads)
file.

**Prerequisites**

* One or more container images available from a local or remote registry.

    !!! Note
        Installing container images from local storage is appropriate only for development and experimental purposes.

**Procedure**

1. Include the `qm` stage in your manifest to initialize the QM partition. Add `content` and `container_images` sections to
the `qm` stage so that you can define the container images that you want to install:

      ```YAML title="automotive image builder manifest"
      --8<-- "demos/container_qm/container_qm.aib.yml:19:31"
      ```

    !!! note
        To install a container image from local storage, set the `containers-transport: containers-storage` parameter.

1. Optional. If you have container configuration files that you want to copy to the image, use the `add_files` section within the `content` stage.
For example, copy the example `radio.container` and `engine.container` files to the image:

      ```YAML title="automotive image builder manifest"
      --8<-- "demos/quadlet_radio_engine/quadlet_radio_engine.aib.yml:25:29"
      ```

    !!! note
        The `source_path:` option resolves a relative path. In this example, the
        container configuration files are in the `../demos` directory.

**Next steps**

Now that you have included your containerized application in your automotive image builder manifest, you can build your AutoSD image.
For more information, see [Building an AutoSD image](building_an_os_image.md#building-an-autosd-image).
