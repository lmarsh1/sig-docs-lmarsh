# Using non-containerized OSbuild

To configure your system, install OSBuild and other related tools. The Automotive SIG uses newer versions of these tools than what are generally
available in the distributions, so you must configure a custom repository.

!!! important
    CentOS Stream 8 is no longer supported by Red Hat and should not be used for development purposes.
    CentOS Stream 10 is currently in a pre-beta state with potential instabilities in packages. If you are using CentOS Stream 10, either roll back
    to an earlier package, track an existing problem report, or open a new problem report on [issues.redhat.com](https://issues.redhat.com/).

**Prerequisites**

- CentOS Stream 9 or 10, Fedora, or RHEL 8 or 9 installed on your host machine

**Procedure**

1. Enable the `osbuild` repo:

    ```console
    dnf copr enable @osbuild/osbuild-stable
    dnf copr enable @centos-automotive-sig/osbuild-auto
    ```

1. Install `osbuild` and related packages:

    ```console
    dnf install osbuild osbuild-auto osbuild-ostree osbuild-tools
    ```
