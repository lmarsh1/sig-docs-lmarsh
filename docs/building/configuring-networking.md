# Configuring networking

Configure a static network for your automotive operating system (OS) image.

**Prerequisites**

* A custom OS image manifest

**Procedure**

1. Add SSH packages to your manifest:

    ```console title="Extra SSH RPMs"
    --8<-- "demos/networking/networking.mpp.yml:1:7"
    ```

1. Configure the `static_ip` variable overrides:

    ```console title="Example static network configuration"
    --8<-- "demos/networking/networking.mpp.yml:8:15"
    ```

1. Enable SSH `root` access:

    ```console title="SSH root access configuration"
    --8<-- "demos/networking/networking.mpp.yml:50:55"
    ```

1. Enable the SSH service through `systemd`:

    ```console title="SSH service managed by systemd"
    --8<-- "demos/networking/networking.mpp.yml:56:59"
    ```

## Next steps

1. Configure the [firewall](configuring-the-firewall.md) for your OS image.
1. Configure interprocess communications (IPCs) between your [ASIL](configuring_communication_asil_containers.md) and
   [QM](configuring_communication_qm_containers.md) containers and [across partitions](configuring_communication_qm_and_asil_containers.md).
1. [Encrypt](enabling_encryption_on_rootfs.md) your filesystem.
1. [Enable BlueChi orchestration](enabling_and_configuring_bluechi.md).
1. [Build your automotive OS image](building_an_os_image.md) with `kernel-automotive` or your
   [custom kernel](building_an_os_image_that uses_a_custom_kernel.md).
