# Configuring the firewall

Configure the firewall for your automotive operating system (OS) image.

**Prerequisites**

* A custom OS image manifest that has network configurations, such as the one you used in
  [Configuring networking](configuring-networking.md)

**Procedure**

1. Add SSH packages to your manifest:

    ```console title="SSH RPM packages for firewall support"
    --8<-- "demos/firewall/firewall.mpp.yml:1:7"
    ```

1. Add the `firewalld` package:

    ```console title="Firewall RPM package"
    --8<-- "demos/firewall/firewall.mpp.yml:35:39"
    ```

1. Configure the firewall with SSH `root` access:

    ```console title="Example firewall, SSH, and port configuration"
    --8<-- "demos/firewall/firewall.mpp.yml:43:56"
    ```

1. Enable firewall and SSH services to run through `systemd`:

    ```console title="Firewall and SSH services managed by systemd"
    --8<-- "demos/firewall/firewall.mpp.yml:58:63"
    ```

## Next steps

1. Configure interprocess communications (IPCs) between your
   [ASIL](configuring_communication_asil_containers.md) and
   [QM](configuring_communication_qm_containers.md) containers and
   [across partitions](configuring_communication_qm_and_asil_containers.md).
1. [Encrypt](enabling_encryption_on_rootfs.md) your filesystem.
1. [Enable BlueChi orchestration](enabling_and_configuring_bluechi.md).
1. [Build your automotive OS image](building_an_os_image.md) with `kernel-automotive` or your
   [custom kernel](building_an_os_image_that%20uses_a_custom_kernel.md).
