# Introducing automotive-image-builder

[Automotive image builder](https://gitlab.com/CentOS/automotive/src/automotive-image-builder) is a
tool used to create operating system (OS) images.
It relies on the capabilities of [OSBuild](https://www.osbuild.org/) underneath it. Because OSBuild
uses files called *manifests* as input to define how to build the image, that terminology
extends to the input files used by automotive-image-builder as well.

The automotive image builder manifests are YAML files that define the content and configuration
of the OS image. A manifest file can be as simple as the following minimal manifest example:

```console title="Minimal AIB manifest"
--8<-- "demos/minimal/minimal.aib.yml"
```

To review available manifest options and configurations, see:
[Automotive Image Builder manifest format](https://centos.gitlab.io/automotive/src/automotive-image-builder/simple_manifest.html)

## Installing Automotive image builder

Automotive Image Builder can be installed in two ways:

* As RPM on RHEL, CentOS, Fedora and derivatives.
* As a container on other Linux distribution or MacOS.

### Installing Automotive image builder from RPM

Automotive image builder is packaged as RPM, available for RHEL, CentOS, and Fedora in the
[centos-automotive-sig/automotive-image-builder](https://copr.fedorainfracloud.org/coprs/g/centos-automotive-sig/automotive-image-builder/)
copr repository. Complete the following steps to install Automotive image builder on these systems:

1. Enable the Automotive image builder and osbuild-auto repositories:

    ```console
    $ sudo dnf copr enable @centos-automotive-sig/automotive-image-builder
    $ sudo dnf copr enable @centos-automotive-sig/osbuild-auto
    ```

1. Install `automotive-image-builder`. This RPM also includes `automotive-image-runner`, a convenient wrapper
around QEMU that you can use to launch OS images as virtual machine, as well as other tools that the build process relies on.

    ```console
    $ sudo dnf install automotive-image-builder
    ```

Next, you can start building images. For example, use the following command to build the `minimal.aib.yml` manifest that is available in
[sig-docs/demos/minimal](https://gitlab.com/CentOS/automotive/sig-docs/-/blob/main/demos/minimal/minimal.aib.yml?ref_type=heads):

```console
$ automotive-image-builder build --distro autosd9 --mode image --target qemu --export qcow2 minimal.aib.yml minimal.qcow2
```

### Running Automotive image builder from a container

The Automotive SIG maintains a container image that you can use to run `automotive-image-builder` in the
[automotive-image-builder Quay.io repo](https://quay.io/repository/centos-sig-automotive/automotive-image-builder?tab=tags). This
containerized build process is useful for hosts other than RHEL, CentOS, and Fedora, or if you do not want to install the RPM-based
version of the `automotive-image-builder` tool.

To easily run `automotive-image-builder` from a container, you can use the `auto-image-builder.sh` script that is available in the
[`sample-images repository](https://gitlab.com/CentOS/automotive/sample-images/-/blob/main/auto-image-builder.sh?ref_type=heads).

1. Download the script:

    ```console
    $ curl -O "https://gitlab.com/CentOS/automotive/sample-images/-/raw/main/auto-image-builder.sh?ref_type=heads"
    ```

1. Build an image. In this example, build the `minimal.aib.yml` manifest that is available in
[sig-docs/demos/minimal](https://gitlab.com/CentOS/automotive/sig-docs/-/blob/main/demos/minimal/minimal.aib.yml?ref_type=heads):

    ```console
    $ bash auto-image-builder.sh build --distro autosd9 --mode image --target qemu --export qcow2 minimal.aib.yml minimal.qcow2
    ```

For more information about the container, see the
[automotive-image-builder](https://gitlab.com/CentOS/automotive/src/automotive-image-builder/) repository.

## Automotive image builder example manifests

The Automotive SIG maintains two collections of `automotive-image-builder` manifests that you can use to better understand the requisite
YAML syntax, and for you to build and modify yourself:

* The [sig-docs/demos](https://gitlab.com/CentOS/automotive/sig-docs/-/tree/main/demos?ref_type=heads) which
  includes all the manifests present in this documentation site. These images are built and tested nightly, and
  when changes are merged to Automotive image builder itself.
* The [sample-images repos](https://gitlab.com/CentOS/automotive/sample-images) which includes a collection
  of manifests. These images are not regularly tested and while most of them are included in
  `sig-docs/demos` there may still be interesting manifests examples.

### mpp.yml vs aib.yml

As you browse through the example manifests you might notice two types of manifests, some ending with `.mpp.yml`
and some ending with `.aib.yml`. They reflect an evolution to the manifest format. The `.mpp.yml` manifests
are written using the first generation format and structure heavily based on [OSBuild](https://www.osbuild.org/)'s
manifests. That format is complex, error-prone and cumbersome when manually edited.

This, first, complex format has been replaced by a "simplified" format which has a limited and clearly defined
set of capabilities documented at: [https://centos.gitlab.io/automotive/src/automotive-image-builder/simple_manifest.html](https://centos.gitlab.io/automotive/src/automotive-image-builder/simple_manifest.html).
This format requires the `.aib.yml` file extension. It is the format that is used in this documentation
and the format that we strongly encourage you to use.

## Understanding the Automotive image builder options

When you build an image with `automotive-image-builder`, you must specify some options in the build command:

```console
$ sudo automotive-image-builder build --mode <package-or-image> --target <target> \
--export <export-format> <path-to-manifest>.mpp.yml <my-image>.qcow2
```

### Architecture

`--arch`
: The hardware architecture to build for (x86_64 or aarch64). If unspecified, the native
architecture is used.

!!! Note
    You can `compose` an image for any architecture, but you can only `build` for the native architecture.

### Distributions

`--distro`
: Define the package repositories for the distribution you intend to use. The default is "cs9". View available distributions with the
  `automotive-image-builder list-dist` command. Available distributions include:

* autosd or autosd9
* cs9
* eln
* f40

  To extend the `list-dist` with custom distributions, add an `ipp.yml` in a directory called `/some/dir/distro` and pass
  `--include /some/dir` to the argument list.

### Modes

`--mode`
: Set the value to `package` or `image`. The default value is `image`.

* Use `package` to build a package-based OS image, which is useful for development and testing.
* Use `image` to build an OSTree image for use in production.

### Targets

`--target`
: The physical or virtual deployment target for your image. The default value is `qemu`.
  View the available targets with the `automotive-image-builder list-targets` command. Available targets include:

* QEMU with aboot or grub
* KVM
* TI AM62, AM69, BeaglePlay, J784S4 EVM, and TDA4
* AWS
* Qualcomm QDrive3 and RideSX4
* ccimx93
* Windows PC
* Renesas R-Car
* Raspberry Pi 4
* NXP S32G2-VNP-RDB3

### Export formats

`--export`
: The image file type that you want to build. View the available export formats with the `automotive-image-builder list-exports` command.
  Export formats available as of October 2024 include:

* **image:** A raw disk image with partitions
* **qcow2:** A qcow2 format disk image with partitions
* **ext4:** An ext4 filesystem containing just the rootfs partition (i.e., no boot partitions)
* **aboot:** An android boot system partition image and a boot partition
* **container:** A container image you can run with podman or docker
* **tar:** A tar file containing the basic rootfs files
* **ostree-commit:** An ostree repo with the commit built from the image
* **ostree-oci-image:** An oci image wrapping the ostree commit from ostree-commit
* **rpmlist:** A json file listing all the RPMs used in the image

## Next steps

For more information about using the `automotive-image-builder` tool with build options and manifests
to provision your hardware, see the flashing guide specific to your target hardware:

* [NXP](../provisioning/flashing-nxp.md)
* [Qualcomm](../provisioning/flashing-images-ride4.md)
* [Renasas](../provisioning/flashing-renesas.md)
* [Texas Instruments](../provisioning/flashing-texas-instruments.md)
