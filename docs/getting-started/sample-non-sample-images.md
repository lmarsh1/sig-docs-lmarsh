# Pre-built OS images

In an effort to continuously deliver working OS images, the Automotive SIG builds
several OS images for AArch64 or x86_64 systems or VMs, which you can find in the
[AutoSD nightly folder](https://autosd.sig.centos.org/AutoSD-9/nightly/).
These images are updated as the CentOS Stream or AutoSD RPM repositories change.

For more information about the Automotive image builder manifests used to build these sample OS images, see
[Introducing automotive-image-builder](about-automotive-image-builder.md), and
[Deploying software on AutoSD](../building/deploying_sw_in_the_os_image.md).

!!! NOTE
    "Package" images are based on RPM packages rather than OSTree file system trees.

!!! IMPORTANT
    Do not use sample or non-sample images in production.

## AutoSD sample images

The [`AutoSD-9/nightly/sample-images` folder](https://autosd.sig.centos.org/AutoSD-9/nightly/sample-images/) contains images built only with
AutoSD RPMs. The images are available for both aarch64 and x86_64.

The naming convention for these image files is: `auto-osbuild-$target-$distro-$image_name-$mode.$arch.$file_type`.
For example, the file name `auto-osbuild-qemu-autosd9-developer-package.x86_64.qcow2` represents the following values:

- `target`: qemu

- `distro`: autosd9

- `image name`: developer

- `mode`: package

- `arch`: x86_64

- `file_type`: qcow2

**Minimal OSTree**
: These OSTree-based QEMU images are examples of a minimal image and have neither a package manager nor SSH:

    - `auto-osbuild-qemu-autosd9-minimal-ostree-aarch64`
    - `auto-osbuild-qemu-autosd9-minimal-ostree-x86_64`

**Developer OSTree and regular**
: These OSTree- or RPM-based QEMU images include standard developer tools for building and working with RPMs, images, OSTree binaries,
  and containers:

    - `auto-osbuild-qemu-autosd9-developer-ostree-aarch64`
    - `auto-osbuild-qemu-autosd9-developer-ostree-x86_64`
    - `auto-osbuild-qemu-autosd9-developer-package-aarch64`
    - `auto-osbuild-qemu-autosd9-developer-pacakge-x86_64`

**QA OSTree**
: These OSTree-based QEMU images built for quality assurance testing are a minimal image with
  extra packages such as: SSH, beaker, rsync, sudo, wget, time, nfs-utils, git, jq:

    - `auto-osbuild-qemu-autosd9-qa-ostree-aarch64`
    - `auto-osbuild-qemu-autosd9-qa-ostree-x86_64`

## AutoSD non-sample images

The [`AutoSD-9/nightly/non-sample-images` folder](https://autosd.sig.centos.org/AutoSD-9/nightly/non-sample-images/) contains x86_64 and
aarch64 images built from AutoSD RPMs and customized with packages from other RPM repositories, such as CentOS Stream 9
and [COPR](https://copr.fedorainfracloud.org/):

**Container OSTree**
: These are OSTree-based QEMU images built to demonstrate how to include containers in images:

    - `auto-osbuild-qemu-autosd9-container-ostree-aarch64`
    - `auto-osbuild-qemu-autosd9-container-ostree-x86_64`
