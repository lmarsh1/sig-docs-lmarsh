# CentOS Build System (CBS)

Automotive SIG and AutoSD packages are built by the CentOS Build System ([CBS](https://cbs.centos.org/)). The procedure to
[contribute RPM packages](contributing-packages.md) is similar for both the Automotive SIG and AutoSD. The only difference is the
CBS tags used, which classify packages in the build system based on their destination.

CBS appends the root tag to reflect one of the following four targets, which specifies where each RPM package
is routed according to its stage of acceptance:

`-build`
: where the packages are built

`-candidate`
: where the packages land after they are built

`-testing`
: where the packages are tested

`-release`
: where stable, released packages land

## Automotive SIG CBS tags

**automotive9s-packages-main-el9s**
: This is the tag you specify when using the `cbs build` command for RPM packages based on CentOS Stream 9
intended for the main Automotive SIG RPM repository.

    * [automotive9s-packages-main-el9s-build](https://cbs.centos.org/koji/taginfo?tagID=2463)
    * [automotive9s-packages-main-candidate](https://cbs.centos.org/koji/taginfo?tagID=2460)
    * [automotive9s-packages-main-testing](https://cbs.centos.org/koji/taginfo?tagID=2461)
    * [automotive9s-packages-main-release](https://cbs.centos.org/koji/taginfo?tagID=2462)

**automotive9s-packages-experimental-el9s**
: This is the tag you specify when using the `cbs build` command for experimental RPM packages based on CentOS Stream 9
intended for the main Automotive SIG RPM repository.

    * [automotive9s-packages-experimental-el9s-build](https://cbs.centos.org/koji/taginfo?tagID=2536)
    * [automotive9s-packages-experimental-candidate](https://cbs.centos.org/koji/taginfo?tagID=2533)
    * [automotive9s-packages-experimental-testing](https://cbs.centos.org/koji/taginfo?tagID=2534)
    * [automotive9s-packages-experimental-release](https://cbs.centos.org/koji/taginfo?tagID=2535)

**automotive10s-packages-main-el10s**
: This is the tag you specify when using the `cbs build` command for RPM packages based on CentOS Stream 10
intended for the main Automotive SIG RPM repository.

    * [automotive10s-packages-main-el10s-build](https://cbs.centos.org/koji/taginfo?tagID=2946)
    * [automotive10s-packages-main-candidate](https://cbs.centos.org/koji/taginfo?tagID=2943)
    * [automotive10s-packages-main-testing](https://cbs.centos.org/koji/taginfo?tagID=2944)
    * [automotive10s-packages-main-release](https://cbs.centos.org/koji/taginfo?tagID=2945)

## AutoSD CBS tags

**autosd9s-packages-main-el9s**
: This is the tag you specify when using the `cbs build` command for RPM packages based on CentOS Stream 9
intended for the main AutoSD RPM repository.

    * [autosd9s-packages-main-el9s-build](https://cbs.centos.org/koji/taginfo?tagID=2609)
    * [autosd9s-packages-main-candidate](https://cbs.centos.org/koji/taginfo?tagID=2606)
    * [autosd9s-packages-main-testing](https://cbs.centos.org/koji/taginfo?tagID=2607)
    * [autosd9s-packages-main-release](https://cbs.centos.org/koji/taginfo?tagID=2608)

**autosd10s-packages-main-el10s**
: This is the tag you specify when using the `cbs build` command for RPM packages based on CentOS Stream 9
intended for the main AutoSD RPM repository.

    * [autosd10s-packages-main-el10s-build](https://cbs.centos.org/koji/taginfo?tagID=2942)
    * [autosd10s-packages-main-candidate](https://cbs.centos.org/koji/taginfo?tagID=2939)
    * [autosd10s-packages-main-testing](https://cbs.centos.org/koji/taginfo?tagID=2940)
    * [autosd10s-packages-main-release](https://cbs.centos.org/koji/taginfo?tagID=2941)
