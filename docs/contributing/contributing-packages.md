# Contributing packages to Automotive repositories

There are three ways to contribute automotive-related packages to AutoSD:

* Package the application for AutoSD, and submit it to the CentOS Automotive SIG.
* Package the application for Fedora, and bring it to EPEL.
* Contact your partner manager to discuss incorporating the application into Red Hat In-Vehicle OS.

You might find it helpful to understand how code flows into and through AutoSD before determining which contribution method is right for you.
For more information about AutoSD and its upstream and downstream relationships,
see [AutoSD upstream and downstream](../about/up-downstreams.md).

## Contributing packages to the Automotive SIG GitLab repository

Clone and fork the Automotive SIG repository, so you can contribute code to AutoSD by submitting a merge request to Automotive SIG maintainers.

**Prerequisites**

* git

### Cloning an Automotive project repository

**Procedure**

1. From the [Automotive SIG repository](https://gitlab.com/centos/automotive/), choose a project repository.
2. Select **Code**, and copy the value from **Clone with HTTPS**.
2. Paste the value into the following command:

    ```console
    git clone https://gitlab.com/CentOS/automotive/<project>.git
    ```

    !!! note
        GitLab has several methods you can use to clone repositories. These example commands only show the HTTPS method.
        For more information about working with GitLab through your terminal, the graphical user interface (GUI),
        or your preferred integrated development environment (IDE), see the [GitLab Docs](https://docs.gitlab.com/ee/topics/git/clone.html).

### Creating a fork

**Procedure**

1. From GitLab, select **Fork**.
2. Optional: Enter a project name. If you don't enter a project name, the fork inherits the name of the origin.
3. Select your name from the **Namespace** menu.
4. Optional: Enter a project description.
5. Select your preferred visibility level.
6. Select **Fork project** to create your fork.

### Adding your fork as a remote

**Procedure**

1. From GitLab, select **Code**, and copy the value from **Clone with HTTPS**.
2. From a terminal window, go to your [local clone](#cloning-an-automotive-project-repository),
   and add your fork as a remote repository:

    ```console
    git remote add <my-fork> https://gitlab.com/CentOS/automotive/<project>.git
    ```

3. Optional: Confirm that you added your fork as a remote:

    ```console
    git remote -v
    ```

    !!! note

        You can run this command at any time to view details about all of the remotes in the cloned repository.

### Submitting a merge request to the Automotive SIG project repository

**Procedure**

1. Create a feature branch on your fork:

    ```console
    git checkout -b <my-branch>
    ```

2. Push updates to your fork:

    ```console
    git push <my-fork> <my-branch>
    ```

3. From the project repository you chose from the [Automotive SIG repository](https://gitlab.com/centos/automotive/), select **New merge request**.
4. Select the `main` branch as the target.
5. Select both of the following merge options:

      * Delete source branch when merge request is accepted.
      * Squash commits when merge request is accepted.

6. Request a review from a maintainer who can review and merge your changes.

## Contributing packages upstream to the CentOS Build System

Code submitted to the Automotive SIG RPM repository must be under an
[approved free and open source license](https://docs.fedoraproject.org/en-US/legal/allowed-licenses/).

Similar to the relationship between Fedora, CentOS Stream, and RHEL, when you build in the Automotive SIG RPM repository,
you're building against AutoSD, which is the future of Red Hat In-Vehicle OS.

The contribution process for the Automotive SIG RPM repository is somewhat simpler than the EPEL process
because the Automotive SIG controls the process. Unlike EPEL, the Automotive SIG repository can override packages
that are in RHEL or AutoSD. This allows SIG members to experiment with new versions or different configurations than those in Red Hat products.

To request a package, send an email to the [CentOS Automotive SIG](https://lists.centos.org/hyperkitty/list/automotive-sig@lists.centos.org/)
mailing list to present the project you want to package in the Automotive SIG RPM repository. After they receive your email, the Automotive SIG will
create a subgroup or project for you in the [CentOS Automotive SIG RPMs repo](https://gitlab.com/CentOS/automotive/rpms/).

**Prerequisites**

* `lookaside_upload_sig` script from [centos-git-common](https://git.centos.org/centos-git-common/blob/master/f/lookaside_upload_sig)

* `centos-packager` to build packages using CentOS Build System [(CBS)](https://cbs.centos.org/)

**Procedure**

1. Run `git add` and `git commit` to add the spec file and patches to your repo.

1. Upload the tarball of the project sources to the lookaside cache:

    ```console
    lookaside_upload_sig -f <tarball> -n <pkg_name>
    ```

    !!! note

        The lookaside cache stores a copy of all upstream archives used when building RPMs. This allows the Automotive SIG to reproduce a build,
        even if the upstream project's website disappears. After your sources are uploaded to the lookaside cache, create a `sources` file in your
        git repository to enable the build system to identify the correct archive to retrieve from the lookaside cache.

1. Create a `sources` file in your repo in the same directory as the spec file:

    ```console
    sha512sum --tag <tarball> > sources
    ```

1. Push your spec file, `sources` file, and any patches to GitLab.

1. The first time you package your code, add your package to the tags you want to build
against:

    ```console
    cbs add-pkg --owner=<username> <tag> <pkg_name>
    ```

    !!! note

        You must do this for the `-candidate`, `-testing` and `-release` tags. For more information, see
        [Automotive SIG CBS tags](cbs.md#automotive-sig-cbs-tags).

1. Build the package using CBS:

    ```console
    cbs build <tag> git+https://gitlab.com/centos/automotive/rpms/<pkg_name>.git#<git_hash>
    ```

    Alternatively, to build the latest commit of the branch you're in:

    ```console
    cbs build <tag> git+https://gitlab.com/centos/automotive/rpms/<pkg_name>.git#`git log -1 --format=format:"%H"`
    ```

    !!! note
        For more information about build tags, see [Automotive SIG CBS tags](cbs.md#automotive-sig-cbs-tags)

1. After the build succeeds, push it to the testing repository:

    ```console
    cbs tag <testing_tag> <pkg_name-version-release>
    ```

    Using the Automotive SIG testing repository for CentOS Stream 9 as an example:

    ```console
    cbs tag automotive9s-packages-main-testing <pkg_name-version-release>
    ```

    !!! note
        This makes the package show up in the [CentOS Buildlogs mirror](https://buildlogs.centos.org/9-stream/automotive/).

1. Push the package to the stable repository in the CentOS mirror network:

    ```console
    cbs tag <release_tag> <pkg_name-version-release>
    ```

    Using the Automotive SIG release repository for CentOS Stream 9 as an example:

    ```console
    cbs tag automotive9s-packages-main-release <pkg_name-version-release>
    ```

    !!! note

        This makes the package show up in the
        [CentOS Stream mirror](http://mirror.stream.centos.org/SIGs/9-stream/automotive/).

## Contributing packages to Fedora and EPEL

To contribute packages to [Fedora and EPEL](index.md#fedora-and-epel-repositories), you must become a
[Fedora package maintainer](https://docs.fedoraproject.org/en-US/package-maintainers/Joining_the_Package_Maintainers/), and
follow the package process to publish your software on [Koji](https://koji.fedoraproject.org/koji/) or
[Copr](https://copr.fedorainfracloud.org/) as described in
the [Fedora Project user documentation site](https://docs.fedoraproject.org/en-US/docs/).

!!! note
    EPEL packages cannot override RHEL packages. If you want a package that already exists in RHEL
    but with a different configuration or version, you must build your package so that it
    does not conflict with RHEL packages.

See the Fedora docs for more information about
[Using the Koji build system](https://docs.fedoraproject.org/en-US/package-maintainers/Using_the_Koji_Build_System/)
and [Publishing RPMs on Copr](https://docs.fedoraproject.org/en-US/quick-docs/publish-rpm-on-copr/).

If you are a visual learner, the Fedora Community publishes videos about
[how to add a package to the official Fedora repositories](https://www.youtube.com/watch?v=w3e3W00KqVI) on the
[Fedora Project YouTube channel](https://www.youtube.com/@fedora).
